"""
author: Jean Gasnier <jean.gasnier@math.u-bordeaux.fr>
Created: June 21, 2022 `git log -- utils.py | tail -5`
Contains auxiliary functions and tools
"""
# from sage.all_cmdline import *   # import sage library
# import_statements(...)
from sage.rings.integer_ring import ZZ
from sage.rings.integer import Integer
from sage.rings.rational_field import QQ
from sage.rings.number_field.number_field import NumberField
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.rings.finite_rings.integer_mod_ring import IntegerModRing
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.rings.fast_arith import prime_range

from sage.functions.other import ceil, floor, sqrt
from sage.arith.misc import gcd, factor, CRT_list
from sage.arith.functions import lcm
from sage.misc.functional import log, cyclotomic_polynomial
from sage.misc.misc import union
from sage.misc.misc_c import prod
from sage.misc.prandom import choice, randint


QQx = QQ['x']; (x,) = QQx._first_ngens(1)
ZZy = ZZ['y']; (y,) = ZZy._first_ngens(1)

class DiskRep():
    """
    A DiskRep object represents the set of integers that are contained in one of the disks in 'self.disks[p]' for every
    prime in 'self.primes'. It can be used to represent the set of integers at which a polynomial takes integer values.

    Attributes:
        'self.primes' is a list of distinct prime integers 'p'
         that are used to describe the set of integers, which are the prime divisors of the period of the set of integers.

        'self.disks' is a dictionnary matching a prime integer 'p' in 'self.primes' to a sorted list of pairs of integers
         [(j_1, a_1), (j_2, a_2), ..., (j_s, a_s)], where a pair '(j, a)' represents the disk of center 'a' and of radius 1/p**j 
         in Zp, or equivalently the elements of Zp that are equal to 'a' modulo 'p**j'.
         We can see 'self.disks[p]' as the union of the disks inside it.

    Methods:
        is_reduced: None --> bool
            Computes if the DiskRep object is reduced, i.e. if it is the DiskRep object with the minimum number of nodes describing a set of integers.

        intersect_disks: list*list*integer --> list   (static)
            Computes the intersection of two unions of p-adic disks.

        self.intersect:  DiskRep --> DiskRep
            Returns the intersection of self with the argument.
    """

    def __init__(self, primes:list, disks:dict):
        self.primes = primes
        self.disks = disks
        for p in self.primes:
            self.disks[p].sort()
    
    def is_reduced(self):
        """
        Computes if self is reduced, i.e. if it is the DiskRep object with the minimum number of nodes describing a set of integers. 
        """
        res = True
        for p in self.primes:
            condp = res
            disks = self.disks[p]
            if disks[0] == (0,0): # Largest disk
                continue
            n = len(disks)
            i = -1
            checked = [0]*n
            while condp and i>-n-1:
                if not checked[i]:
                    checked[i]=1
                    k,a = disks[i]
                    count = 1  #counts disks which are siblings of k,a
                    j = i-1
                    while count < p and j > -n-1 and disks[j][0]==k:
                        b = disks[j][1]
                        if (b%p**(k-1))==(a%p**(k-1)):
                            count+=1
                            checked[j]=1
                        j-=1
                    condp = (count<p)
                    i-=1
            res = condp
        return res             

    @staticmethod
    def intersect_disks(cov1:list, cov2:list, p:int):
        """
        cov1: Union of p-adic disks [(j, a)].
        cov2: Union of p-adic disks [(k, b)].
        p: a prime integer.

        Returns the intersection of cov1 and cov2.
        """
        if not cov1:
            return []
        elif not cov2:
            return []
        elif cov1[0] == (0,0):
            return cov2
        elif cov2[0] == (0,0):
            return cov1
        res = []
        for (j,a) in cov1:
            for (k,b) in cov2:
                if j > k and a%(p**k) == b: # x = a mod p^j is 'more precise' than x = b mod p^k, ex: x = 1 mod 9 and x = 1 mod 3
                    res.append((j,a))
                elif j <= k and b%(p**j) == a: # x = a mod p^j is 'less precise' than x = b mod p^k, ex: x = 1 mod 3 and x = 1 mod 9
                    res.append((k,b))
        res.sort() # sort according to ascending powers p^j
        return res

    def intersect(self, other):
        """
        other: a DiskRep object.
        
        Returns the intersection of 'self' and 'other'.
        """
        all_primes = union(self.primes, other.primes) # union of two lists of primes

        disks = {} # empty dictionary
        for p in all_primes:
            if not p in self.primes:
                disks[p] = other.disks[p]
            elif not p in other.primes:
                disks[p] = self.disks[p]
            else:
                disks[p] = DiskRep.intersect_disks(self.disks[p],other.disks[p],p)

        return DiskRep(all_primes, disks)
    
    def p_include(self,other,p):
        """
        p: a prime integer
        other: a DiskRep object.
        
        Returns True if the p-adic disks of self includes covers the p-adic disks of other, False otherwise.
        Only works if the DiskRep object is reduced, i.e. if it is the DiskRep object with the minimal number of nodes representing a set of solutions.
        """
        cov1 = self.disks[p]
        cov2 = other.disks[p]
        if not cov2:
            return True
        elif not cov1:
            return False
        elif cov1[0] == (0,0):
            return True
        elif cov2[0] == (0,0):
            return False
        length1 = len(cov1)
        length2 = len(cov2)
        res = True
        ind = 0
        while res and ind < length2 :
            (j,a) = cov2[ind]
            cond = False
            count = 0
            while (not cond) and count < length1:
                (k,b) = cov1[count]
                cond = j >= k and a%(p**k) == b # x = a mod p^j is 'more precise' than x = b mod p^k, ex: x = 1 mod 9 and x = 1 
                count+=1
            res = res and cond
            ind+=1
        return res
    
    def __str__(self):
        return "Diskrep("+str(self.primes)+', '+str(self.disks)+')'
    
    def __repr__(self):
        return "Diskrep("+str(self.primes)+', '+str(self.disks)+')'

def rep_prime(P, diskrep_P, print_prime=False):
    """
    Input : 'P' an univariate polynomial with rational coefficients which is non-constant, has positive leading coefficient,
            is irreducible and takes integer values at diskrep_P.
            'diskrep_P' is a DiskRep object.
            'print_prime' is an option to print the primes which divide every integer value of P

    Returns True/False whether the polynomial P represent primes.

    >>> rep_prime((x**2+x+2)/2)
    True
    """
    
    primes = diskrep_P.primes # primes 'p' setting a congruence condition such that P takes integer values
    chinese_remainders = diskrep_P.disks # the congruence conditions modulo the primes 'p'

    no_arg_int = 1
    period = 1
    for p in primes:
        period *= p**chinese_remainders[p][-1][0] # accumulate all highest powers p^max_j for all p
        sum_p = 0
        for j,_ in chinese_remainders[p]: # what happens here?
            sum_p += p**(chinese_remainders[p][-1][0]-j) # sum(p**(max_j-j))
        no_arg_int *= sum_p

    # here, check at some random values x_i that there is no systematic small factor of P(x_i)
    d = 0
    if no_arg_int >= 20: # develop a strategy to get an idea of the scene without listing everything
        for _ in range(20):
            remainders = []
            modules = []
            for p in primes:
                power, remainder = choice(chinese_remainders[p])
                if power < chinese_remainders[p][-1][0]:
                    i = randint(0,p-1)
                    remainders.append(remainder+i*p**power)
                    modules.append(p**(power+1))
                else:
                    remainders.append(remainder)
                    modules.append(p**power)

            j = CRT_list(remainders, modules)
            d = gcd(d,P(j))

    else:
        for counter in range(20//no_arg_int):
            for _ in range(20):
                remainders = []
                modules = []
                for p in primes:
                    power, remainder = choice(chinese_remainders[p])
                    if power < chinese_remainders[p][-1][0]:
                        i = randint(0,p-1)
                        remainders.append(remainder+i*p**power)
                        modules.append(p**(power+1))
                    else:
                        remainders.append(remainder)
                        modules.append(p**power)

                j = CRT_list(remainders, modules)
                d = gcd(d,P(j+counter*period))

    assert d.denom() == 1
    d = ZZ(d)
    
    cond5 = True
    if d > 1: # there is a problem: a systematic factor d of P(x)
        for p in d.prime_divisors():
            if not p in primes and p <= P.degree():

                F = GF(p)
                poly_ring = PolynomialRing(F, names=('zmod',)); (zmod,) = poly_ring._first_ngens(1)
                
                if poly_ring(P) % (zmod**p-zmod) == 0: # P always has a root at any element of GF(p), for example P=x*(x+1) mod 2
                    if print_prime:
                        print("Detected problematic prime for rep_prime("+str(P)+"):",p)
                    cond5 = False
            else:
                denom_P = lcm([pj.denom() for pj in P.list()])
                P0 = P*denom_P
                val_p_m = denom_P.valuation(p)
                _, diskrep_P0_p, _ = rep_int_v2(P0/p**(val_p_m+1))
                if diskrep_P0_p.p_include(diskrep_P,p):
                    if print_prime:
                        print("Detected problematic prime for rep_prime("+str(P)+"):",p)
                    cond5 = False

    return cond5

def Hensel(start, poly, deriv, prec, prime):
    """
    Performs Hensel lift from value start for polynomial poly up to precision prec in caracteristic prime

    start : integer
    poly : integer polynomial
    deriv : derivative of poly
    prec : integer
    prime : prime integer
    """
    alpha = start
    
    coeff = 1 # prec = 1
    F = GF(prime)

    while poly(alpha) % (prime**prec) != 0:
        t = ZZ( -F(poly(alpha)/(prime**coeff)) / F(deriv(alpha)) )
        alpha += t*prime**coeff
        coeff +=1
    return alpha

def find_curves(Q, R, T, n_sec, diskrep, powers, bound_low, bound_high, limit=1):
    """
    Input : Q, R, T, are univariate polynomials describing a complete family of pairing-friendly elliptic curves. 

            'n_sec' is an integer describing the lower bound for the size of 'r' in bits.

            'diskrep' is a DiskRep object representing the integers in which Q, R, T, Y and H take integer values, where Y and H are
             the remaining polynomials in the family.
            
            'powers' is a dictionary matching every prime p in diskrep.primes to the maximum power j for (j,a) in diskrep.disks[p].
            
            'bound_low' and 'bound_high' are integers limiting the range of the search. If a bound is not a multiple of the
            period of diskrep, we use the multiple right under it instead.
            
            'limit' is the number of curves to look for (integer).

    Returns a list of elements of the form (q, r, t, x_0) where 'q' is a prime integer, 'r' is a prime integer, 't' is an integer coprime to 'q', and 'x' is the integer such that q=Q(x_0), t=T(x_0).
    They represent a subgroup of prime order 'r' of an elliptic curve in the family Q, R, T. 
    """
    C = prod(prime_range(10**7))

    disks, primes = diskrep.disks, diskrep.primes
    denomi = 1
    moduli = []
    for p in primes:
        denomi *= p**powers[p]
        moduli.append(p**powers[p])
    
    nbr_per_p = {}
    size_disks={}
    n_int = 1
    for p in primes:
        temp = 0
        size_disks_p = []

        for (j,_) in disks[p]:
            size_disks_p.append( p**(powers[p]-j) )
            temp += p**(powers[p]-j)

        size_disks[p] = size_disks_p
        nbr_per_p[p] = temp
        n_int *= temp
    
    found = 0
    res = []
    count = floor(bound_low/denomi )
    stop = floor(bound_high/denomi)

    while found < limit and count < stop:
        i=0
        while i < n_int:
            store_i = i
            remainders = []
            for p in primes:
                ip = i % nbr_per_p[p]
                i = i//nbr_per_p[p] 
                j = 0
                while ip >= size_disks[p][j]:
                    ip-= size_disks[p][j]
                    j+=1
                remainders.append(disks[p][j][1]+ip*p**disks[p][j][0])
            for signe in [1,-1]:
                x_0 = CRT_list(remainders, moduli) + signe*count*denomi
                q = Q(x_0)
                assert q.denom() == 1
                q = ZZ(q)
                
                if q.is_pseudoprime() and q.is_prime() : 
                    r = R(x_0)
                    assert r.denom() == 1
                    r = ZZ(r)
                    g = gcd(r,C)
                    rprime = r
                    while g > 1:
                        rprime = rprime // g
                        g = gcd(rprime,g)
                        
                    if rprime >= 2**n_sec and rprime.is_pseudoprime() and rprime.is_prime():
                        
                        t = T(x_0)
                        assert t.denom() == 1
                        found += 1
                        res.append((q,rprime,t,x_0))
            i = store_i+1
        count += 1
    
    if found == limit:
        print("Search successful.")
    else:
        print("Search unsuccessful or uncomplete. You may try to find more curves by adding 1 to start_margin.")
    return res

def rep_int_v2(poly):
    """
    Input : 'poly' an univariate polynomial with rational coefficients.

    Returns a tuple (bool, argint_DiskRep, powers) where

    'bool' is a boolean representing if the polynomial P takes integer values at integers.
    
    'argint_DiskRep' is a DiskRep object representing the integers x such that poly(x) is an integer.

    'powers' is a dictionary matching the primes 'p' which divide the denominator of poly and the valuation of the denominator
     in 'p'.

    >>> rep_int_v2((x**2+x+2)/2)
        (True, Diskrep([], {}), {2: 1})

    """

    m = lcm([pj.denom() for pj in poly.list()])

    factorisation = factor(m)
    integral_poly = ZZy(m*poly)

    primes = []
    disks = {}
    powers = {}

    boole = True

    for prime, power in factorisation:
        res = []
        lift_roots_rec(integral_poly, prime, power, res)
        powers[prime] = power
        if (len(res) and res[0][0]) or not len(res): #  res!=[(0,0)]
            primes.append(prime)
            disks[prime] = res
            disks[prime].sort()
            if not disks[prime]:
                boole = False
    return boole, DiskRep(primes,disks), powers

def lift_roots_rec(poly, p, k, res, root=0, depth=0):
    """
    Input : 'poly' a monic irreducible univariate polynomial with integral coefficients
            'p' a prime number
            'k' an integer
            'root' an integer
            'depth' an integer
            'res' a list of pairs of integer

    Recursively compute the roots of poly mod p**k, and store them in res (by side effect).
    """
    nu = poly.content().valuation(p)
    polynu = ZZy(poly/p**nu)
    m = mu(polynu,p)
    if k <= nu+m :
        res.append((depth, root))
    else :
        
        for zero, multi in polynu.roots(GF(p)):
            if multi == 1 :
                rprime = Hensel(ZZ(zero), polynu, polynu.derivative(), k-nu, p)
                lift_roots_rec(polynu, p, 0, res, root+rprime*p**depth, depth+k-nu)
            else:
                lift_roots_rec(polynu(ZZ(zero)+p*y), p, k-nu, res, root+ZZ(zero)*p**depth, depth+1)

def mu(P,p):
    """
    Computes mu(P)
    """
    x = P.parent().gens()[0]
    d = P.degree()
    basis = [1]
    factor = 1
    for i in range(d):
        factor *= (x-i)/(i+1)
        basis.append(factor)
    basis.reverse()
    val = []
    for i in range(len(basis)):
        quotient,remainder = P.quo_rem(basis[i])
        val.append(ZZ(quotient).valuation(p))
        P = remainder
    return min(val)