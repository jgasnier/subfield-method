"""
Author Jean Gasnier
"""
from utils import *

def subfield_method_6(k,coeffs):
    """
    Implements the subfield method with F the 6-th cyclotomic field
    and K the lcm(6,k)-th cyclotomic field
    for theta = (coeffs[1]*zeta_6 + coeffs[0])*zeta_l

    INPUT:
    - `k`: embedding degree
    - `coeffs`: a list [a_0, a_1] of coefficients in ZZ, alpha = a_0 + a_1*zeta_6

    Returns the univariate polynomials Q, R, T, Y, H.
    """
    l = lcm(6,k)
    e = l//6
    D = 3
    K = NumberField(cyclotomic_polynomial(l), names=('zeta_l',)); (zeta_l,) = K._first_ngens(1)

    phi_k = cyclotomic_polynomial(k)
    zeta_6 = zeta_l**e

    alpha = QQx(coeffs)(zeta_6) # alpha = a0 + a1*w
    theta = zeta_l*alpha        # theta = zeta_l * alpha, not zeta_k * alpha
    # define beta so that beta * zeta_l = zeta_k
    if k % 6 == 1:
        beta = zeta_6**5
    elif k % 6 == 2:
        beta = zeta_6**2
    elif k % 6 == 3:
        beta = -1
    elif k % 6 == 4:
        beta = zeta_6**4
    elif k % 6 == 5:
        beta = zeta_6
    else:# k = 0 mod 6 and l=k
        beta = 1
    
    assert phi_k(beta*zeta_l) == 0
    
    assert not (theta**e).is_rational()
    coord = (theta**e).coordinates_in_terms_of_powers()
    # now coord is a function that will compute the input coordinates in basis (theta^e) instead of zeta_l

    P1 = QQx(coord( beta/alpha ))

    P2 =  QQx(coord( beta / (alpha*sqrt(K(-D))) ))
    P3 =  QQx(coord( 1/sqrt(K(-D)) ))

    T = P1(x**e)*x+1
    Y = P2(x**e)*x-P3(x**e)
    R = theta.minpoly(algorithm="pari")

    Q = (T**2+D*Y**2)/4
    assert (Q+1-T) % R == 0
    H = QQx( (Q+1-T)//R )

    return Q, R, T, Y, H

def subfield_method_4(k,coeffs):
    """
    Implements the subfield method with F the 4-th cyclotomic field
    and K the lcm(4,k)-th cyclotomic field
    for theta = (coeffs[1]*zeta_4 + coeffs[0])*zeta_l
    
    INPUT:
    - `k`: embedding degree
    - `coeffs`: a list [a_0, a_1] of coefficients in ZZ, alpha = a_0 + a_1*zeta_4

    Returns the univariate polynomials Q, R, T, Y, H.
    """
    l = lcm(4,k)
    e = l//4
    D = 1
    K = NumberField(cyclotomic_polynomial(l), names=('zeta_l',)); (zeta_l,) = K._first_ngens(1)

    phi_k = cyclotomic_polynomial(k)
    zeta_4 = zeta_l**e

    alpha = QQx(coeffs)(zeta_4) # alpha = a0 + a1*i
    theta = zeta_l*alpha        # theta = zeta_l * alpha, not zeta_k * alpha
    # define beta so that beta * zeta_l = zeta_k
    if k % 4 == 1:
        beta = zeta_4**3 # beta = -zeta_4
    elif k % 8 == 2:
        beta = zeta_4
    elif k % 8 == 6:
        beta = zeta_4**3
    elif k % 4 == 3:
        beta = zeta_4
    else:# k = 0 mod 4 and l=k
        beta = 1
    
    assert phi_k(beta*zeta_l) == 0
    
    assert not (theta**e).is_rational()
    coord=(theta**e).coordinates_in_terms_of_powers()
    # now coord is a function that will compute the input coordinates in basis (theta^e) instead of zeta_l

    P1= QQx(coord( beta/alpha ))

    P2 =  QQx(coord( beta/(alpha*sqrt(K(-D))) ))
    P3 =  QQx(coord( 1/sqrt(K(-D)) ))

    T = P1(x**e)*x+1
    Y = P2(x**e)*x-P3(x**e)
    R = theta.minpoly(algorithm="pari")

    Q = (T**2+D*Y**2)/4
    assert (Q+1-T) % R == 0
    H = QQx( (Q+1-T)//R )

    return Q, R, T, Y, H

def subfield_method_D1_D3(k, coeffs, D):
    """
    A function for Figure 4 that handles D=3 and D=1 as Case 3, Case 2 or Case 1 according to k
     - D=3, Case 3 with d = lcm(k,6) if 3 | k
     - D=3, Case 2 if 3 does not divide k and k even
     - D=3, Case 1 if 3 does not divide k and k odd
     - D=1, Case 3 with d=4 if 4 | k
     - D=1, Case 2 if k = 2 mod 4
     - D=1, Case 1 if k = 1 mod 2

    INPUT:
    - `k`: embedding degree
    - `coeffs`: a list [a_0, a_1] so that alpha = a_0 + a_1*omega
    - `D`: discriminant, can be 1 or 3

    Returns the univariate polynomials Q, R, T, Y, H.
    """
    if D == 3:
        if (k % 6) == 0:       # case d, d=6
            d = 6
            l = k
            e = k // 6
        elif (k % 6) == 3:     # case d, d=3
            d = 3
            l = k
            e = k // 3
        elif (k % 6) in [2, 4]:# case 2
            d = 6
            l = 3*k
            e = k // 2
        else:                  # case 1
            assert (k % 6) in [1, 5]
            d = 3
            l = 3*k
            e = k
    elif D == 1:
        if (k % 4) == 0:       # case d, d=4
            d = 4
            l = k
            e = k // 4
        elif (k % 4) == 2:     # case 2
            d = 4
            l = 2*k
            e = k // 2
        else:                  # case 1
            assert (k % 2) == 1
            d = 4
            l = 4*k
            e = k
    else:
        raise ValueError("Error D = {} but only D=1 or D=3 allowed".format(D))
    assert e*d == l and (l % k) == 0
    K = NumberField(cyclotomic_polynomial(l), names=('zeta_l',)); (zeta_l,) = K._first_ngens(1)
    sqrt_D = (K(-D)).sqrt()
    # there is no need to enumerate over conjugates of zeta_l
    zeta_d = zeta_l**e
    phi_k = cyclotomic_polynomial(k)
    # QQx is defined in utils.py
    alpha = QQx(coeffs)(zeta_d)
    zeta_k = zeta_l**(l // k) # where (l // k) = 1 (case-d) or 2, 3, 4 (cases 1, 2)
    assert phi_k(zeta_k) == 0
    theta = alpha*zeta_k
    theta_e = theta**e
    if theta_e.is_rational():
        return
    R = theta.minpoly()
    mycoord = (theta_e).coordinates_in_terms_of_powers()
    P1 = QQx(mycoord(1/alpha))
    P2 = QQx(mycoord(1/(alpha * sqrt_D)))
    P3 = QQx(mycoord(1/sqrt_D))

    T = QQx(x * P1(x**e)) + 1
    Y = QQx(x * P2(x**e)) - P3(x**e)

    Q = (T**2+D*Y**2)/4
    assert (Q+1-T) % R == 0
    assert (phi_k(T-1) % R) == 0
    H = QQx((Q+1-T)//R)

    return Q, R, T, Y, H

def subfield_method_D(k,coeffs,D):
    """
    Implements the subfield method with K = QQ[sqrt(-D)] and L = QQ[sqrt(-D), zeta_k] where zeta_k is a k-th primitive root of unity,
    for theta = (coeffs[1]*omega+coeffs[0])*zeta_k and omega = (-1+sqrt(-D))/2 if D=3 mod 4, omega = sqrt(-D) otherwise.
    sqrt(-D) should not be an element of the k-th cyclotomic field.

    INPUT:
    - `k`: embedding degree
    - `coeffs`: a list [a_0, a_1] so that alpha = a_0 + a_1*omega
    - `D`: positive square-free discriminant different from 1 or 3

    Returns the univariate polynomials Q, R, T, Y, H.
    """
    if (D % 4) == 3:
        K = NumberField([cyclotomic_polynomial(k),x**Integer(2)+x+(D+1)//4], names=('zeta_k', 'omega',)); (zeta_k, omega,) = K._first_ngens(2)
        sqrt_D = 2*omega+1
    else:
        K = NumberField([cyclotomic_polynomial(k),x**Integer(2)+D], names=('zeta_k', 'omega',)); (zeta_k, omega,) = K._first_ngens(2)
        sqrt_D = omega
    if k % 2: # that is, if (k % 2) == 1:
        # k odd, (k % 2) == 1, case 1, e = k
        e = k
    else:
        # k even, (k % 2) == 0, case 2, e = k/2
        e = k//2

    alpha = QQx(coeffs)(omega)
    theta = zeta_k*alpha

    if (theta**e).is_rational(): # can happen if a=1 and b=0
        return None
    coord=(theta**e).coordinates_in_terms_of_powers()

    P1= QQx(coord( 1/alpha ))

    P2 =  QQx(coord( 1/(alpha*sqrt_D) ))
    P3 =  QQx(coord( 1/sqrt_D ))

    T = P1(x**e)*x+1
    Y = P2(x**e)*x-P3(x**e)
    R = theta.absolute_minpoly()

    Q = (T**2+D*Y**2)/4
    assert (Q+1-T) % R == 0
    H = QQx((Q+1-T)//R)

    return Q, R, T, Y, H

def to_latex(poly,file_output=None):
    """
    Input : 'poly' a univariate polynomial with rational coefficients.
            'file_output' an optional file object in 'w' or 'a' mode.

    Outputs a string coding the polynomial in Latex. Also writes it in the file output if need so.
    """
    denomi = lcm([qj.denom() for qj in poly.list()])
    
    if denomi != 1:
        poly = denomi * poly
        string = "\\frac{{1}}{{{}}}\\left(".format(denomi)
    else:
        string=""

    for i in range(poly.degree()):
        coeff = ZZ(poly.list()[-i-1])
        if coeff>0:
            if i>0 :
                string+="+"
            if coeff==1 and poly.degree()-i !=1:
                if poly.degree()-i < 10:
                    string += "X^{}".format(poly.degree()-i)
                else:
                    string += "X^{{{}}}".format(poly.degree()-i)
            elif coeff==1 :
                string += "X"
            elif coeff == 0:
                continue
            elif poly.degree()-i ==1:
                string += "{}X".format(coeff)
            else:
                if poly.degree()-i < 10:
                    string += "{}X^{}".format(coeff,poly.degree()-i)
                else:
                    string += "{}X^{{{}}}".format(coeff,poly.degree()-i)
        elif coeff<0:
            coeff=-coeff
            if coeff==1 and poly.degree()-i !=1:
                if poly.degree()-i < 10:
                    string += "-X^{}".format(poly.degree()-i)
                else:
                    string += "-X^{{{}}}".format(poly.degree()-i)
            elif coeff==1 :
                string += "-X"
            elif poly.degree()-i ==1:
                string += "-{}X".format(coeff)
            else:
                if poly.degree()-i < 10:
                    string += "-{}X^{}".format(coeff,poly.degree()-i)
                else:
                    string += "-{}X^{{{}}}".format(coeff,poly.degree()-i)
    
    p0=ZZ(poly(0))
    if p0>0 and denomi!=1:
        string+="+{} \\right) ".format(p0)
    elif p0<0 and denomi!=1:
        string+="{} \\right) ".format(p0)
    elif denomi!=1 : 
         string+="\\right) "
    elif p0>0:
        string+="+{} ".format(p0)
    elif p0<0:
        string+="{} ".format(p0)
    if file_output is None:
        print(string)
    else:
        file_output.write(string+'\n')

def increment_signed_int(b):
    if b <= 0:
        b = -b + 1
    else:
        b = -b
    return b

def increment_unsigned_int(b):
    if b < 0:
        b = b - 1
    else:
        b = b + 1
    return b

def find_all_poly(k,method=subfield_method_6,D=None,bound_a=10,bound_b=10,latex=False,file_output=None,verbose=False):
    """
    Input:  'k' is an integer representing the desired embedding degree.
            'method' is the method to be used to generate the polynomials among subfield_method_6, subfield_method_4, subfield_method_D.
            'D' is the discriminant of the produced families. It is necessary to give its value when using method "subfield_method_D".  
            'bound_a' and 'bound_b' are bounds on the coefficients a,b considered in the enumeration over alpha = (b*omega + a)
            'latex' is an option to also write the polynomials found in latex format.
            'file_output' is the name of the file where to store the results. It is opened in "w" mode.
            'verbose' controls if text is printed in the terminal.

    Enumerates polynomials generated with the chosen method for all the theta = (b*omega + a)*zeta_k within the range given such that gcd(a,b)=1.
    Outputs every family found.
    Returns the list of successful coefficients (a,b) and a dictionary matching (a,b) with its corresponding DiskRep object.
    """
    success=[]
    smallest_denoms = []
    disk_representations = {}

    if method == subfield_method_4:
        D = 1
    elif method == subfield_method_6:
        D = 3
    elif D is None:
        raise ValueError("Giving the desired discriminant D is necessary when using method 'subfield_method_D'.")

    if method == subfield_method_D:
            method = lambda k,coeffs:subfield_method_D(k,coeffs,D)
    elif method == subfield_method_D1_D3:
            method = lambda k,coeffs:subfield_method_D1_D3(k,coeffs,D)

    if file_output is not None:
        fic = open(file_output,'w')

    a = 0
    while abs(a) <= bound_a:
        if a == 0:
            b = 1
            bound_bi = 1
        else:
            b = 0 # (a,b) = (1,0) is ok, it corresponds to theta = zeta_k and some FST 6.6 or FST 6.2, 6.3, 6.4 for example
            bound_bi = bound_b
        while abs(b) <= bound_bi:
            if gcd(a,b) != 1:
                b = increment_signed_int(b)
                continue 
            coeffs = (ZZ(a), ZZ(b))
            if verbose:
                print("Coefficients:",coeffs)
            try:
                ans = method(k, coeffs)
                if ans is None:
                    b = increment_signed_int(b)
                    continue
                Q, R, T, Y, H = ans
                if Q.is_irreducible():
                    boole, diskrep, powers = compute_diskrep(Q,T,Y,D)
                    if boole and rep_prime(Q, diskrep):
                        if file_output is not None:
                            fic.write("Coefficients: "+str(coeffs)+'\n')
                            fic.write("Polynomials found:\n")

                            fic.write("T="+str(T)+'\n')
                            fic.write("Y="+str(Y)+'\n')
                            fic.write("R="+str(R)+'\n')
                            fic.write("Q="+str(Q)+'\n\n')
                            fic.flush()
                            if latex:
                                to_latex(T,fic)
                                to_latex(Y,fic)
                                to_latex(R,fic)
                                to_latex(Q,fic)
                                fic.write('\n')
                                fic.flush()
                        if verbose:
                            print("Polynomials found (a,b) = ({},{}):".format(a,b))
                            print("T=",T)
                            print("R=",R)
                            print("Q=",Q)
                            print("Y=",Y)
                            print("")
                            if latex:
                                to_latex(T)
                                to_latex(Y)
                                to_latex(R)
                                to_latex(Q)
                                print("")
                        success.append(coeffs)
                        disk_representations[coeffs] = diskrep

                        denom_repint = 1
                        for p in diskrep.primes:
                            denom_repint *= p**powers[p]

                        if len(smallest_denoms) == 0:
                            smallest_denoms = [(denom_repint, coeffs, Q, R, T, Y, H)]
                        else:
                            smallest_denoms.append((denom_repint, coeffs, Q, R, T, Y, H))

                    elif not boole:
                        prod = 1
                        problematic_primes = []
                        for prime in diskrep.primes:
                            if not diskrep.disks[prime]:
                                prod*=prime
                                problematic_primes.append(prime)
                        if a*prod <= bound_a and b*prod <= bound_b:
                            if verbose:
                                print("Polynomials do not satisfy all conditions but some multiple of the generator could be successful, the problematic primes were",problematic_primes,"(No common integer seed)\n")
                        else:
                            if verbose:
                                print("Polynomials do not satisfy all conditions.(No common integer seed)\n")
                    else:
                        if verbose:
                            print("Polynomials do not satisfy all conditions.(Prime representation)\n")
                else:
                    if verbose:
                        print("Polynomials do not satisfy all conditions.(irreducibility)\n")

            except (AssertionError, ValueError) as exception:
                if verbose:
                    print(exception)
                    print("Coefficients do not satisfy requirements.\n")

            b = increment_signed_int(b)
        a = increment_signed_int(a)
    if verbose:
        print("Best ones with smallest denominator:")
    if file_output is not None:
        fic.write("Best ones with smallest denominator:\n")
    smallest_denoms.sort()
    for (denom, coeffs, Q, R, T, Y, _) in smallest_denoms:
        a,b = coeffs
        facto = factor(denom)
        if verbose:
            print("d={}={}\na={} b={}\nT={}\nY={}\nR={}\nQ={}\n\n".format(denom, facto , a, b, T,Y,R,Q))
        if file_output is not None:
            fic.write("d={}={}\na={} b={}\nT={}\nY={}\nR={}\nQ={}\n\n\n".format(denom, facto , a, b, T,Y,R,Q))
    
    if file_output is not None:
        fic.write("Positive results for coefficients " + str(success)+'\n')
        fic.close()
    if verbose:
        print("Positive results for coefficients",success)
    return smallest_denoms, disk_representations

def compute_diskrep(Q,T,Y,D):
    """
    Input : Potential complete family of pairing-friendly elliptic curves represented by univariate polynomials Q,H,T,Y
     and 'D' their discriminant.
    """
    res = True

    if D%4 == 3:
        _, diskrep_T, _ = rep_int_v2(T)
        _, diskrep_Y, _, = rep_int_v2(Y)

        denom_Q = lcm([qj.denom() for qj in Q.list()])
        pow_Q_2 = denom_Q.valuation(2)
        _, diskrep_Q, _ = rep_int_v2(Q*denom_Q/2**pow_Q_2)

        diskrep = diskrep_T.intersect(diskrep_Q.intersect(diskrep_Y))

    else:
        _, diskrep_T, _ = rep_int_v2(T/2)
        _, diskrep_Y, _, = rep_int_v2(Y/2)

        diskrep = diskrep_T.intersect(diskrep_Y)

    powers = {}
    
    for prime in diskrep.primes:
        if not diskrep.disks[prime]:
            res = False
        else:
            powers[prime] = diskrep.disks[prime][-1][0]
    
    return res, diskrep, powers
