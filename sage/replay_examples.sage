QQx.<x> = QQ[]
#############
# Reproducing Example 5 (KSS18) with the hints on alpha
#############
k = 18
D = 3
e = 3 # case 3, 6 | k
K.<zeta_k> = CyclotomicField(k)
omega = zeta_k^3
(2*omega - 1)^2 == -D

sqrt_D = 2*omega-1 # sqrt(K(-D))

a,b = (-3, 1)
alpha = a + b*omega
theta = alpha * zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^4 + 16*x + 7)/7

R=R/343
H=H*343
x0 = 2^80+2^77+2^76-2^61-2^53-2^14
q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [0, 6])
E.order() == h*r

#############
# Reproducing Example 6 with the hints on alpha and (a,b)
#############
k = 22
D = 7
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+2])
# omega <-> (-1+sqrt(-7))/2
# zeta_k is a primitive k-th root of unity
(2*omega+1)^2 == -7
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1
sqrt_D^2 == -D

a, b = (1, 1) # we get the same family with (a, b) = (1, 1) and (0, -1)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^12 + 45*x + 46)/46
x0 = -779523

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))//23^2
assert r.is_prime()
h = ZZ(23^2*H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [-5/7, -2/7])
#E = EllipticCurve(Fq, [-35, 98]) # it is an isomorphic curve
assert E.order() == h*r

#############
# Reproducing Example 7 with the hints on alpha and (a, b)
#############
k = 22
D = 1
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+1])
# omega <-> sqrt(-1)
# zeta_k is a primitive k-th root of unity
omega^2 == -1
e = k//2 # Case 2, k is even
sqrt_D = omega # sqrt(K(-D))

a, b = (1, 2)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (-x^12 - 5148*x + 6605)/6605
R = R/1321
H = H*1321

x0 = 1721613

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))//1321
assert r.is_prime()
h = ZZ(1321*H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [27, 0])
E.order() == h*r

#############
# Reproducing Example 8 with the hints on alpha
#############
k = 22
D = 3
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+1])
# omega <-> (-1+sqrt(-3))/2
# zeta_k is a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (5, 4)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (-x^12 + 18764460*x + 341901)/341901

R = R/(3^10*67)
H = H*3^10*67

x0 = 2183007

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [0, -1])
E.order() == h*r

#############
# Reproducing Example 9 with the hints on alpha
#############
k = 16
D = 35
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+9])
# omega <-> (-1+sqrt(-35))/2
# zeta_k a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (0, 1)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^9 + 424*x + 19431)/19431
R = R/(3^16*17^2*127^2)
H = H*3^16*17^2*127^2

x0 = 24368382

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
a0 = Fq(31)
b0 = Fq(1317728292483273230848411296136454481787303013004556453310885409729582323176053041964253728851893875176832851139310314329240)
E = EllipticCurve(Fq, [a0, b0])
E.order() == h*r

x0 = 465976332

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
a1 = Fq(-3)
b1 = Fq(-353516288775379111156223150872987073049616818343001803308262413473854718389881596417584974519382406492820360401621370710141691809254181620077041208)
E = EllipticCurve(Fq, [a1, b1])
E.order() == h*r

#############
# Reproducing Example 10 with the hints on alpha
#############
k = 18
D = 3
e = 3 # case 3, 6 | k
K.<zeta_k> = CyclotomicField(k)
omega = zeta_k^3
(2*omega - 1)^2 == -D

sqrt_D = 2*omega-1 # sqrt(K(-D))

a,b = (1, 3)
alpha = a + b*omega
theta = alpha * zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)
R = R/(13^3 * 17^2)
H = H * 13^3 * 17^2

assert T == (3*x^4 + 176*x + 221)/221

x0 = -2^81-2^79+2^67-2^55-2^22
q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [0, -3])
E.order() == h*r

#############
# Reproducing Example 11 with the hints on alpha
#############
k = 20
D = 1
e = k//4 # Case 3, 4 | k
K.<zeta_k> = CyclotomicField(k)
omega = zeta_k^e
# omega <-> sqrt(-1)
# zeta_k a primitive k-th root of unity
omega^2 == -D
sqrt_D = omega # sqrt(K(-D))

a,b = (1, -2)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (2*x^6 + 117*x + 205)/205
R = R/25625
H = H*25625

x0 = -(2^49+2^46+2^41+2^18+2^3+2^2+1) # pb

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [2,0])
assert E.order() == r*h

x0 = 2^49+2^46+2^44+2^40+2^34+2^27+2^14+1
q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [2,0])
assert E.order() == r*h

#############
# Reproducing Example 12 with the hints on alpha
#############
k = 20
D = 1
e = k//4 # Case 3, 4 | k
K.<zeta_k> = CyclotomicField(k)
omega = zeta_k^e
# omega <-> sqrt(-1)
# zeta_k a primitive k-th root of unity
omega^2 == -D
sqrt_D = omega # sqrt(K(-D))

a,b = (1, 2)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (-2*x^6 + 117*x + 205)/205
R = R/25625
H = H*25625

x0 = -2^49-2^45-2^42-2^36+2^11+1

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [3,0])
assert E.order() == r*h

x0 = -2^49-2^47+2^45-2^27-2^22-2^18-1

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [2,0])
assert E.order() == r*h

x0 = 2^49+2^46-2^41+2^35+2^30-1

q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [2,0])
assert E.order() == r*h

#############
# Reproducing Example 17 with the hints on alpha
#############
k = 46
D = 7
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+2])
# omega <-> (-1+sqrt(-7))/2
# zeta_k a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (0, 1)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^24 + 2115*x + 1934)/1934
R = R/967
H = H*967

#############
# Reproducing Example 18 with the hints on alpha
#############
k = 46
D = 15
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+4])
# omega <-> (-1+sqrt(-15))/2
# zeta_k a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (1, -1)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^24 + 122762871*x + 2347906338)/2347906338
R = R/(3^22*47^2)
H = H*3^22*47^2

#############
# Reproducing Example 19 (similar to KSS18, same R) with the hints on alpha
#############
k = 18
D = 3
e = 3 # case 3, 6 | k
K.<zeta_k> = CyclotomicField(k)
omega = zeta_k^3
(2*omega - 1)^2 == -D

sqrt_D = 2*omega-1 # sqrt(K(-D))

a,b = (2, -3)
alpha = a + b*omega
theta = alpha * zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (-3*x^4 - 55*x + 7)/7

R=R/343
H=H*343

x0 = -2^80-2^69-2^51-2^3
q = ZZ(Q(x0))
assert q.is_prime()
r = ZZ(R(x0))
assert r.is_prime()
h = ZZ(H(x0))

Fq = GF(q)
E = EllipticCurve(Fq, [0, 3])
E.order() == h*r

#############
# Reproducing Example 20 with the hints on alpha
#############
k = 28
D = 11
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+3])
# omega <-> (-1+sqrt(-11))/2
# zeta_k a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (0, 1)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^15 + 718*x + 3237)/3237

#############
# Reproducing Example 21 with the hints on alpha
#############
k = 40
D = 11
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+3])
# omega <-> (-1+sqrt(-11))/2
# zeta_k a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (-2, 9)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^21 - 1298983332046081026293664*x + 28371069490077576136284995)/28371069490077576136284995

#############
# Reproducing Example 22 with the hints on alpha
#############
k = 40
D = 103
K.<zeta_k, omega> = NumberField([cyclotomic_polynomial(k), x^2+x+26])
# omega <-> (-1+sqrt(-103))/2
# zeta_k a primitive k-th root of unity
(2*omega+1)^2 == -D
e = k//2 # Case 2, k is even
sqrt_D = 2*omega+1 # sqrt(K(-D))

a,b = (1, 3)
alpha = a + b*omega
theta = alpha*zeta_k
coord = (theta^e).coordinates_in_terms_of_powers()

P1 = QQx(coord(1/alpha))
P2 = QQx(coord(1/(alpha*sqrt_D)))
P3 = QQx(coord(1/sqrt_D))

T = P1(x**e)*x+1
Y = P2(x**e)*x-P3(x**e)
R = theta.absolute_minpoly()

Q = (T**2+D*Y**2)/4
assert (Q+1-T) % R == 0
H = QQx((Q+1-T)//R)

assert T == (x^21 + 348732358800946792843017*x + 4202620637716354992437144)/4202620637716354992437144
