from subfield_method import *

# This script shows an example of how our module can be used to compute complete families of elliptic curves
# using the variants of the subfield method described in [article].

# First thing is to set the parameters : the embedding degree k, the method chosen among subfield_method_4, subfield_method_6,
# and subfield_method_D (for this one, you also need to specify D).

k = 22
method = subfield_method_D
D = 7

# Optionally, you can give the name of a file to store the computed families.

file = "k22_method_D_D7.txt"

# Now to compute the families, use the following command:

sorted_by_period, disk_representations = find_all_poly(k, method=method,D=D,file_output=file)

# Note that you can modify the range of the search and ask for the latex code of the generated polynomials.

# You can now try to compute a curve using a generated family. However, there is a mandatory step that needs to be realized 
# manually before continuing: finding a cofactor such that R/cofactor and H*cofactor take integer values at the same points than
# Q, T and Y.

period, coeffs, Q, R, T, Y, H = sorted_by_period[0]
diskrep = disk_representations[coeffs]

# Here we explain how to determine the correct cofactor. First let's have a look at repint and rep_int_v2(H):

# sage: diskrep
# ([2, 23, 7],
#  {2: [(2, 0), (2, 2)],
#   23: [(1, 1),
#    (1, 2),
#    (1, 3),
#    (1, 4),
#    (1, 6),
#    (1, 8),
#    (1, 9),
#    (1, 12),
#    (1, 13),
#    (1, 16),
#    (1, 18)],
#   7: [(1, 1), (1, 6)]}
# )

# sage: rep_int_v2(H)
# (True,
#  {2: [(20, 474956), (20, 573620), (21, 474954), (21, 1622198)],
#   7: [(1, 1), (1, 6)],
#   23: [(2, 64), (2, 66), (2, 463), (2, 465)]},
#  {2: 25, 7: 1, 23: 2},
#  [2, 7, 23])

# We notice that 2 and 23 are problematic so cofactor needs to be a multiple of 2 and 23. Let's try:

cofactor = 2^20*23^2

# We get:

# sage: rep_int_v2(H*cofactor)
# (True, {2: [(2, 0), (2, 2)], 7: [(1, 1), (1, 6)]}, {2: 5, 7: 1}, [2, 7])

# sage:  rep_int_v2(R/cofactor)
# (True,
#  {2: [(1, 0)],
#   23: [(1, 1),
#    (1, 2),
#    (1, 4),
#    (1, 6),
#    (1, 8),
#    (1, 9),
#    (1, 12),
#    (1, 13),
#    (1, 16),
#    (2, 455),
#    (2, 486)]},
#  {2: 20, 23: 2},
#  [2, 23])

# Notice that with this cofactor we obtain results really similar to diskrep. It means that the cofactor is correct.

R = R/cofactor
H = H*cofactor

diskrep = diskrep.intersect(rep_int_v2(R)[1].intersect(rep_int_v2(H)[1]))

powers = {}
for p in diskrep.primes:
    powers[p] = diskrep.disks[p][-1][0]

# Now use this command to search for curves:

results = find_curves(Q,R,T,256,diskrep,powers,0,2**20*period,limit=1)
print(results)
exit